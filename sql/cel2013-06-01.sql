--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: active_admin_comments; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE active_admin_comments (
    id integer NOT NULL,
    resource_id character varying(255) NOT NULL,
    resource_type character varying(255) NOT NULL,
    author_id integer,
    author_type character varying(255),
    body text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    namespace character varying(255)
);


ALTER TABLE public.active_admin_comments OWNER TO cel;

--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE active_admin_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.active_admin_comments_id_seq OWNER TO cel;

--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE active_admin_comments_id_seq OWNED BY active_admin_comments.id;


--
-- Name: attendees; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE attendees (
    id integer NOT NULL,
    user_id integer,
    event_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.attendees OWNER TO cel;

--
-- Name: attendees_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE attendees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attendees_id_seq OWNER TO cel;

--
-- Name: attendees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE attendees_id_seq OWNED BY attendees.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE categories (
    id integer NOT NULL,
    name character varying(255),
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.categories OWNER TO cel;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO cel;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: clients; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE clients (
    id integer NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    company character varying(255),
    website character varying(255),
    email character varying(255),
    phone character varying(255),
    address_one character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    address_two character varying(255),
    city character varying(255),
    state character varying(255),
    zip_code integer,
    banner_size character varying(255),
    company_logo_file_name character varying(255),
    company_logo_content_type character varying(255),
    company_logo_file_size integer,
    company_logo_updated_at timestamp without time zone,
    banner_file_name character varying(255),
    banner_content_type character varying(255),
    banner_file_size integer,
    banner_updated_at timestamp without time zone
);


ALTER TABLE public.clients OWNER TO cel;

--
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clients_id_seq OWNER TO cel;

--
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE clients_id_seq OWNED BY clients.id;


--
-- Name: events; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE events (
    id integer NOT NULL,
    user_id integer,
    short_description character varying(255),
    long_description text,
    location character varying(255),
    address_one character varying(255),
    city character varying(255),
    zipcode integer,
    contact_name character varying(255),
    contact_email character varying(255),
    contact_phone character varying(255),
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    max_num_attendees integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    published boolean DEFAULT false,
    category_id integer,
    custom_url character varying(255),
    video_url character varying(255),
    website character varying(255),
    facebook_url character varying(255),
    twitter_id character varying(255),
    twitter_hashtag character varying(255),
    min_num_attendees integer,
    age_group character varying(255),
    start_registration timestamp without time zone,
    end_registration timestamp without time zone,
    cover_file_name character varying(255),
    cover_content_type character varying(255),
    cover_file_size integer,
    cover_updated_at timestamp without time zone,
    title character varying(255),
    address_two character varying(255),
    state character varying(255),
    is_occurrence boolean DEFAULT false,
    slug character varying(255)
);


ALTER TABLE public.events OWNER TO cel;

--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_id_seq OWNER TO cel;

--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- Name: forem_categories; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE forem_categories (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    slug character varying(255)
);


ALTER TABLE public.forem_categories OWNER TO cel;

--
-- Name: forem_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE forem_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.forem_categories_id_seq OWNER TO cel;

--
-- Name: forem_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE forem_categories_id_seq OWNED BY forem_categories.id;


--
-- Name: forem_forums; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE forem_forums (
    id integer NOT NULL,
    name character varying(255),
    description text,
    category_id integer,
    views_count integer DEFAULT 0,
    slug character varying(255)
);


ALTER TABLE public.forem_forums OWNER TO cel;

--
-- Name: forem_forums_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE forem_forums_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.forem_forums_id_seq OWNER TO cel;

--
-- Name: forem_forums_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE forem_forums_id_seq OWNED BY forem_forums.id;


--
-- Name: forem_groups; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE forem_groups (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.forem_groups OWNER TO cel;

--
-- Name: forem_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE forem_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.forem_groups_id_seq OWNER TO cel;

--
-- Name: forem_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE forem_groups_id_seq OWNED BY forem_groups.id;


--
-- Name: forem_memberships; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE forem_memberships (
    id integer NOT NULL,
    group_id integer,
    member_id integer
);


ALTER TABLE public.forem_memberships OWNER TO cel;

--
-- Name: forem_memberships_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE forem_memberships_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.forem_memberships_id_seq OWNER TO cel;

--
-- Name: forem_memberships_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE forem_memberships_id_seq OWNED BY forem_memberships.id;


--
-- Name: forem_moderator_groups; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE forem_moderator_groups (
    id integer NOT NULL,
    forum_id integer,
    group_id integer
);


ALTER TABLE public.forem_moderator_groups OWNER TO cel;

--
-- Name: forem_moderator_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE forem_moderator_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.forem_moderator_groups_id_seq OWNER TO cel;

--
-- Name: forem_moderator_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE forem_moderator_groups_id_seq OWNED BY forem_moderator_groups.id;


--
-- Name: forem_posts; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE forem_posts (
    id integer NOT NULL,
    topic_id integer,
    text text,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    reply_to_id integer,
    state character varying(255) DEFAULT 'pending_review'::character varying,
    notified boolean DEFAULT false
);


ALTER TABLE public.forem_posts OWNER TO cel;

--
-- Name: forem_posts_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE forem_posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.forem_posts_id_seq OWNER TO cel;

--
-- Name: forem_posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE forem_posts_id_seq OWNED BY forem_posts.id;


--
-- Name: forem_subscriptions; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE forem_subscriptions (
    id integer NOT NULL,
    subscriber_id integer,
    topic_id integer
);


ALTER TABLE public.forem_subscriptions OWNER TO cel;

--
-- Name: forem_subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE forem_subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.forem_subscriptions_id_seq OWNER TO cel;

--
-- Name: forem_subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE forem_subscriptions_id_seq OWNED BY forem_subscriptions.id;


--
-- Name: forem_topics; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE forem_topics (
    id integer NOT NULL,
    forum_id integer,
    user_id integer,
    subject character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    locked boolean DEFAULT false NOT NULL,
    pinned boolean DEFAULT false,
    hidden boolean DEFAULT false,
    last_post_at timestamp without time zone,
    state character varying(255) DEFAULT 'pending_review'::character varying,
    views_count integer DEFAULT 0,
    slug character varying(255)
);


ALTER TABLE public.forem_topics OWNER TO cel;

--
-- Name: forem_topics_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE forem_topics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.forem_topics_id_seq OWNER TO cel;

--
-- Name: forem_topics_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE forem_topics_id_seq OWNED BY forem_topics.id;


--
-- Name: forem_views; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE forem_views (
    id integer NOT NULL,
    user_id integer,
    viewable_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    count integer DEFAULT 0,
    viewable_type character varying(255),
    current_viewed_at timestamp without time zone,
    past_viewed_at timestamp without time zone
);


ALTER TABLE public.forem_views OWNER TO cel;

--
-- Name: forem_views_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE forem_views_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.forem_views_id_seq OWNER TO cel;

--
-- Name: forem_views_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE forem_views_id_seq OWNED BY forem_views.id;


--
-- Name: newsletters; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE newsletters (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    email character varying(255)
);


ALTER TABLE public.newsletters OWNER TO cel;

--
-- Name: newsletters_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE newsletters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.newsletters_id_seq OWNER TO cel;

--
-- Name: newsletters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE newsletters_id_seq OWNED BY newsletters.id;


--
-- Name: pastors; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE pastors (
    id integer NOT NULL,
    bio text,
    phone character varying(255),
    address_one character varying(255),
    address_two character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    profile_pic_file_name character varying(255),
    profile_pic_content_type character varying(255),
    profile_pic_file_size integer,
    profile_pic_updated_at timestamp without time zone,
    user_id integer,
    title character varying(255),
    congregation character varying(255),
    is_active boolean DEFAULT false
);


ALTER TABLE public.pastors OWNER TO cel;

--
-- Name: pastors_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE pastors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pastors_id_seq OWNER TO cel;

--
-- Name: pastors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE pastors_id_seq OWNED BY pastors.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO cel;

--
-- Name: shippings; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE shippings (
    id integer NOT NULL,
    event_id integer,
    name character varying(255),
    short_description character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.shippings OWNER TO cel;

--
-- Name: shippings_events; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE shippings_events (
    shipping_id integer,
    event_id integer
);


ALTER TABLE public.shippings_events OWNER TO cel;

--
-- Name: shippings_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE shippings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shippings_id_seq OWNER TO cel;

--
-- Name: shippings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE shippings_id_seq OWNED BY shippings.id;


--
-- Name: taggings; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE taggings (
    id integer NOT NULL,
    tag_id integer,
    taggable_id integer,
    taggable_type character varying(255),
    tagger_id integer,
    tagger_type character varying(255),
    context character varying(128),
    created_at timestamp without time zone
);


ALTER TABLE public.taggings OWNER TO cel;

--
-- Name: taggings_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE taggings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taggings_id_seq OWNER TO cel;

--
-- Name: taggings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE taggings_id_seq OWNED BY taggings.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE tags (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.tags OWNER TO cel;

--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tags_id_seq OWNER TO cel;

--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: cel; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    provider character varying(255),
    uid character varying(255),
    is_admin boolean DEFAULT false,
    is_active boolean DEFAULT true,
    forem_admin boolean DEFAULT false,
    forem_state character varying(255) DEFAULT 'pending_review'::character varying,
    forem_auto_subscribe boolean DEFAULT false
);


ALTER TABLE public.users OWNER TO cel;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: cel
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO cel;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cel
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY active_admin_comments ALTER COLUMN id SET DEFAULT nextval('active_admin_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY attendees ALTER COLUMN id SET DEFAULT nextval('attendees_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY clients ALTER COLUMN id SET DEFAULT nextval('clients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY forem_categories ALTER COLUMN id SET DEFAULT nextval('forem_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY forem_forums ALTER COLUMN id SET DEFAULT nextval('forem_forums_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY forem_groups ALTER COLUMN id SET DEFAULT nextval('forem_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY forem_memberships ALTER COLUMN id SET DEFAULT nextval('forem_memberships_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY forem_moderator_groups ALTER COLUMN id SET DEFAULT nextval('forem_moderator_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY forem_posts ALTER COLUMN id SET DEFAULT nextval('forem_posts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY forem_subscriptions ALTER COLUMN id SET DEFAULT nextval('forem_subscriptions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY forem_topics ALTER COLUMN id SET DEFAULT nextval('forem_topics_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY forem_views ALTER COLUMN id SET DEFAULT nextval('forem_views_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY newsletters ALTER COLUMN id SET DEFAULT nextval('newsletters_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY pastors ALTER COLUMN id SET DEFAULT nextval('pastors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY shippings ALTER COLUMN id SET DEFAULT nextval('shippings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY taggings ALTER COLUMN id SET DEFAULT nextval('taggings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cel
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: active_admin_comments; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY active_admin_comments (id, resource_id, resource_type, author_id, author_type, body, created_at, updated_at, namespace) FROM stdin;
\.


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('active_admin_comments_id_seq', 1, false);


--
-- Data for Name: attendees; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY attendees (id, user_id, event_id, created_at, updated_at) FROM stdin;
1	3	4	2013-05-10 15:28:47.645692	2013-05-10 15:28:47.645692
2	3	6	2013-05-10 17:10:38.805253	2013-05-10 17:10:38.805253
3	3	9	2013-05-22 14:38:32.15483	2013-05-22 14:38:32.15483
4	15	9	2013-05-26 14:16:11.953946	2013-05-26 14:16:11.953946
5	13	10	2013-06-01 02:44:14.995901	2013-06-01 02:44:14.995901
\.


--
-- Name: attendees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('attendees_id_seq', 5, true);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY categories (id, name, description, created_at, updated_at) FROM stdin;
1	Concert		2013-04-05 02:48:38.868536	2013-04-05 02:48:38.868536
2	Luncheon		2013-04-05 18:58:29.830351	2013-04-05 18:58:29.830351
3	Breakfast		2013-04-05 18:58:54.825694	2013-04-05 18:58:54.825694
4	Revival		2013-04-05 19:00:21.476077	2013-04-05 19:00:21.476077
5	Retreats		2013-04-06 02:55:51.096719	2013-04-06 02:55:51.096719
7	Counselling		2013-04-07 02:21:53.50216	2013-04-07 02:21:53.50216
8	Convention		2013-04-07 02:22:22.977982	2013-04-07 02:22:22.977982
9	Women Day Program		2013-04-07 02:22:53.958645	2013-04-07 02:22:53.958645
10	Men Day Program		2013-04-07 02:23:16.289896	2013-04-07 02:23:16.289896
11	Family and Friends Day		2013-04-07 02:23:48.28695	2013-04-07 02:23:48.28695
6	Church Anniversary		2013-04-07 02:20:51.958758	2013-04-07 02:25:14.072562
12	Pastor Anniversary		2013-04-07 02:25:43.620985	2013-04-07 02:25:43.620985
13	Church Picnic		2013-04-07 02:37:55.284915	2013-04-07 02:37:55.284915
14	Seven Up Program		2013-04-07 02:40:00.900424	2013-04-07 02:40:00.900424
15	Youth Talent Show		2013-04-07 02:41:14.753129	2013-04-07 02:41:14.753129
16	Bible Vacation		2013-04-07 02:42:17.104846	2013-04-07 02:42:17.104846
17	Other		2013-04-07 02:43:32.449276	2013-04-07 02:43:32.449276
18	Healing Service		2013-04-17 14:16:09.569359	2013-04-17 14:16:09.569359
19	Ordination		2013-04-17 14:18:02.52164	2013-04-17 14:18:02.52164
20	Worship Service		2013-04-17 14:19:11.044987	2013-04-17 14:19:11.044987
23	Celebration 		2013-04-17 14:25:36.037377	2013-04-17 14:25:36.037377
24	Fund Raiser		2013-04-17 14:26:35.065807	2013-04-17 14:26:35.065807
\.


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('categories_id_seq', 24, true);


--
-- Data for Name: clients; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY clients (id, first_name, last_name, company, website, email, phone, address_one, created_at, updated_at, address_two, city, state, zip_code, banner_size, company_logo_file_name, company_logo_content_type, company_logo_file_size, company_logo_updated_at, banner_file_name, banner_content_type, banner_file_size, banner_updated_at) FROM stdin;
\.


--
-- Name: clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('clients_id_seq', 1, true);


--
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY events (id, user_id, short_description, long_description, location, address_one, city, zipcode, contact_name, contact_email, contact_phone, start_date, end_date, max_num_attendees, created_at, updated_at, published, category_id, custom_url, video_url, website, facebook_url, twitter_id, twitter_hashtag, min_num_attendees, age_group, start_registration, end_registration, cover_file_name, cover_content_type, cover_file_size, cover_updated_at, title, address_two, state, is_occurrence, slug) FROM stdin;
9	13	Come and celebrate the Launching of Christian Event Link, the premier Christian Social Media Web Site.	<p>Christian Event Link, the premier social media site has arrived, it is the place were every christian will be able to communicate and interact with christian people and where pastors can communicate with their people away from church and other pastors and even more.</p>\r\n	Life Changing Ministry for Jesus	694 SW Bayshore Blvd	Port Saint Lucie	34952	Apostle Rick Carraway/ Apostle Vernon Whitaker/ Shawn Pinkston		772-812-5957/ 561-718-2361	2013-06-02 05:00:00	2013-06-02 07:00:00	\N	2013-05-22 10:15:06.063876	2013-05-26 19:50:45.064491	t	23							\N	Everyone	\N	\N	party.png	image/png	367308	2013-05-22 14:34:35.203557	Christian Event Link Launching Celebration		FL	f	christian-event-link-launching-celebration
8	1	TESTING	<p>Really nice test</p>\r\n	fasdfasdf	asdfasdf	San Diego	91910	Mario R. Vallejo	mario.r.vallejo@gmail.com	334323423	2013-05-31 15:55:00	2013-05-31 10:55:00	\N	2013-05-21 17:56:57.808378	2013-05-22 23:04:57.01646	f	2	\N	\N	\N	\N	\N	\N	\N	25 to 44	\N	\N	IMG_45555144.jpeg	image/jpeg	98208	2013-05-21 17:56:55.678519	TESTING	asdfasdf	CA	f	testing
11	3	\N	\N	\N	\N	\N	\N	\N	\N	\N	2013-05-31 05:25:00	2013-05-31 17:45:00	\N	2013-05-31 20:57:03.374979	2013-05-31 20:57:03.374979	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Just Checking	\N	\N	t	\N
10	62	Youth Retreat		Destiny Church of God International	310 Blount Street  	Tallahassee	\N	Ambassador Deanna Smith	toughlove2010@att.net	772-288-4595	2013-06-07 16:00:00	2013-06-09 10:00:00	\N	2013-05-31 00:39:43.740458	2013-06-01 02:49:20.495568	t	5							\N	Everyone	\N	\N	Youth_Retreat.jpg	image/pjpeg	116012	2013-05-31 00:39:41.476373	Youth Retreat	Suite 109	FL	f	youth-retreat
\.


--
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('events_id_seq', 11, true);


--
-- Data for Name: forem_categories; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY forem_categories (id, name, created_at, updated_at, slug) FROM stdin;
1	General	2013-04-25 18:32:28.582275	2013-04-25 18:32:28.582275	general
\.


--
-- Name: forem_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('forem_categories_id_seq', 6, true);


--
-- Data for Name: forem_forums; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY forem_forums (id, name, description, category_id, views_count, slug) FROM stdin;
6	How Should You Choose Leaders?	What characteristics should leaders have to lead people?	1	2	how-should-you-choose-leaders
5	Church Growth	How to increase church membership	1	23	church-growth
3	Resources	We talk about religious resources	1	47	resources
2	Retreats	We talk about religious retreats	1	38	retreats
\.


--
-- Name: forem_forums_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('forem_forums_id_seq', 7, true);


--
-- Data for Name: forem_groups; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY forem_groups (id, name) FROM stdin;
1	Preachers
\.


--
-- Name: forem_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('forem_groups_id_seq', 1, true);


--
-- Data for Name: forem_memberships; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY forem_memberships (id, group_id, member_id) FROM stdin;
\.


--
-- Name: forem_memberships_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('forem_memberships_id_seq', 1, false);


--
-- Data for Name: forem_moderator_groups; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY forem_moderator_groups (id, forum_id, group_id) FROM stdin;
\.


--
-- Name: forem_moderator_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('forem_moderator_groups_id_seq', 1, false);


--
-- Data for Name: forem_posts; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY forem_posts (id, topic_id, text, user_id, created_at, updated_at, reply_to_id, state, notified) FROM stdin;
\.


--
-- Name: forem_posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('forem_posts_id_seq', 19, true);


--
-- Data for Name: forem_subscriptions; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY forem_subscriptions (id, subscriber_id, topic_id) FROM stdin;
1	3	1
2	3	2
3	13	3
4	13	4
7	15	6
8	15	7
11	15	8
12	15	5
13	3	9
14	3	10
\.


--
-- Name: forem_subscriptions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('forem_subscriptions_id_seq', 14, true);


--
-- Data for Name: forem_topics; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY forem_topics (id, forum_id, user_id, subject, created_at, updated_at, locked, pinned, hidden, last_post_at, state, views_count, slug) FROM stdin;
\.


--
-- Name: forem_topics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('forem_topics_id_seq', 10, true);


--
-- Data for Name: forem_views; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY forem_views (id, user_id, viewable_id, created_at, updated_at, count, viewable_type, current_viewed_at, past_viewed_at) FROM stdin;
24	15	8	2013-04-29 20:01:00.618568	2013-04-29 20:03:50.139862	6	Forem::Topic	2013-04-29 20:01:00.618026	2013-04-29 20:01:00.618026
16	15	2	2013-04-28 03:14:13.431615	2013-04-28 03:14:13.526376	1	Forem::Topic	2013-04-28 03:14:13.431197	2013-04-28 03:14:13.431197
19	13	5	2013-04-28 03:36:02.464651	2013-04-29 20:27:34.979018	3	Forem::Forum	2013-04-29 20:27:34.977834	2013-04-28 03:36:02.464231
13	13	1	2013-04-28 03:12:09.271025	2013-04-29 20:58:04.05713	8	Forem::Forum	2013-04-29 20:58:04.056215	2013-04-28 22:15:58.41989
7	13	1	2013-04-27 03:28:02.835207	2013-04-28 22:23:58.795098	4	Forem::Topic	2013-04-28 22:23:58.794202	2013-04-27 03:28:02.83473
28	13	9	2013-05-16 09:57:56.16157	2013-05-16 09:57:56.246383	1	Forem::Topic	2013-05-16 09:57:56.150205	2013-05-16 09:57:56.150205
14	15	1	2013-04-28 03:13:36.239259	2013-04-30 02:15:09.979954	19	Forem::Forum	2013-04-30 02:06:21.897143	2013-04-29 19:50:28.056457
29	1	6	2013-05-20 14:36:41.894254	2013-05-20 14:36:42.154833	1	Forem::Forum	2013-05-20 14:36:41.882583	2013-05-20 14:36:41.882583
18	15	5	2013-04-28 03:19:47.063052	2013-04-30 02:22:00.276791	9	Forem::Topic	2013-04-30 02:15:13.263901	2013-04-29 19:50:35.503738
1	1	3	2013-04-25 18:34:08.224905	2013-05-20 14:36:51.177593	6	Forem::Forum	2013-05-20 14:36:46.703325	2013-04-26 20:44:17.626609
11	13	3	2013-04-28 03:01:01.216603	2013-04-28 03:32:25.668878	11	Forem::Forum	2013-04-28 03:32:25.668105	2013-04-28 03:01:01.216171
17	13	4	2013-04-28 03:15:03.919958	2013-04-29 13:19:32.094544	11	Forem::Topic	2013-04-29 13:19:32.093541	2013-04-28 03:15:03.919569
6	1	1	2013-04-26 20:44:21.904454	2013-05-20 14:36:53.222377	2	Forem::Topic	2013-05-20 14:36:53.22109	2013-04-26 20:44:21.810355
30	13	6	2013-05-20 20:14:51.862993	2013-05-20 20:14:51.9167	1	Forem::Forum	2013-05-20 20:14:51.861463	2013-05-20 20:14:51.861463
8	13	2	2013-04-27 03:32:05.885963	2013-05-20 20:19:01.296787	28	Forem::Forum	2013-05-20 20:19:01.294713	2013-05-09 01:49:00.110456
9	13	2	2013-04-27 03:32:14.072564	2013-05-20 20:19:12.469893	18	Forem::Topic	2013-05-20 20:19:12.468943	2013-05-09 01:49:09.317456
20	15	4	2013-04-28 03:41:06.56397	2013-04-28 03:43:15.219326	3	Forem::Topic	2013-04-28 03:41:06.563567	2013-04-28 03:41:06.563567
27	3	9	2013-05-10 17:15:29.147863	2013-05-21 21:04:52.656627	9	Forem::Topic	2013-05-21 21:04:00.825466	2013-05-16 14:32:07.674584
25	15	1	2013-04-30 02:13:41.288369	2013-05-04 19:59:29.836758	3	Forem::Topic	2013-05-04 19:59:29.835858	2013-04-30 02:13:41.256117
26	3	5	2013-05-10 17:15:03.498321	2013-05-21 21:05:03.991236	7	Forem::Forum	2013-05-21 21:03:49.920394	2013-05-16 14:32:04.124462
22	15	6	2013-04-29 19:58:34.608698	2013-04-29 19:58:34.612866	1	Forem::Topic	2013-04-29 19:58:34.587506	2013-04-29 19:58:34.587506
3	3	1	2013-04-26 17:27:47.404082	2013-05-21 21:05:14.329317	7	Forem::Topic	2013-05-21 21:05:14.326945	2013-05-10 17:14:19.357229
12	13	3	2013-04-28 03:02:30.628032	2013-04-28 03:13:03.558382	5	Forem::Topic	2013-04-28 03:02:30.62764	2013-04-28 03:02:30.62764
2	3	3	2013-04-26 15:01:26.311337	2013-05-21 21:05:20.918601	8	Forem::Forum	2013-05-21 21:05:10.434088	2013-05-10 17:14:06.28639
23	15	7	2013-04-29 20:00:11.212147	2013-04-29 20:00:11.215049	1	Forem::Topic	2013-04-29 20:00:11.211759	2013-04-29 20:00:11.211759
10	15	3	2013-04-28 02:55:27.361455	2013-05-07 03:33:36.10151	22	Forem::Forum	2013-05-07 03:33:27.818137	2013-05-04 19:59:25.505667
15	15	2	2013-04-28 03:14:05.479248	2013-05-07 16:04:16.846008	7	Forem::Forum	2013-05-07 16:04:16.83864	2013-04-30 02:14:53.515118
5	3	2	2013-04-26 17:38:07.238791	2013-05-21 21:05:30.56013	2	Forem::Topic	2013-05-21 21:05:30.558825	2013-04-26 17:38:07.238226
4	3	2	2013-04-26 17:37:01.90352	2013-05-21 21:05:35.734382	3	Forem::Forum	2013-05-21 21:05:27.135593	2013-04-26 17:37:01.902894
21	15	5	2013-04-29 19:50:46.765751	2013-05-10 01:08:20.244342	13	Forem::Forum	2013-05-10 01:08:07.835804	2013-05-04 19:58:48.057415
32	3	10	2013-05-21 21:07:45.994325	2013-05-21 21:08:08.276666	2	Forem::Topic	2013-05-21 21:07:45.993754	2013-05-21 21:07:45.993754
31	3	7	2013-05-21 21:07:38.82823	2013-05-21 21:08:11.150054	4	Forem::Forum	2013-05-21 21:07:38.809145	2013-05-21 21:07:38.809145
\.


--
-- Name: forem_views_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('forem_views_id_seq', 32, true);


--
-- Data for Name: newsletters; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY newsletters (id, created_at, updated_at, email) FROM stdin;
1	2013-05-19 21:44:49.979892	2013-05-19 21:44:49.979892	
2	2013-05-21 21:11:32.998381	2013-05-21 21:11:32.998381	shawn.pinkston@almondev.com
3	2013-05-28 20:57:47.109209	2013-05-28 20:57:47.109209	apostledoc7@aol.com
\.


--
-- Name: newsletters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('newsletters_id_seq', 3, true);


--
-- Data for Name: pastors; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY pastors (id, bio, phone, address_one, address_two, created_at, updated_at, profile_pic_file_name, profile_pic_content_type, profile_pic_file_size, profile_pic_updated_at, user_id, title, congregation, is_active) FROM stdin;
1	<p>Greetings, I'm Apostle&nbsp;Ricky Carraway&nbsp;the Senior Pastor of Life Changing Ministries of Jesus Inc. along with my wife Shannon, we are located in Port Saint Lucie, FL.&nbsp;34952.&nbsp;The idea of Christian Event Link came to&nbsp;me after meditating on a change of paste. During this period of meditation, God gave me the idea of putting together a website that would bring Christians together&nbsp;and give pastors away to&nbsp;provide information on their&nbsp;worship services and ministry plans. It is my sincere hope that you will find this media site a blessing as well as informative.</p>	772 812-5957	694 SW Bayshore Blvd, Port Saint Lucie, FL 34952		2013-04-17 14:45:35.032982	2013-05-23 16:33:31.501284	Rick_Shannon.jpg	image/jpeg	52618	2013-05-06 12:17:29.647236	13	Pastor	Non Denomination	t
3	I have a bio.&nbsp; It is interesting.<br>	8880-555-1212	3137 Hyde Park	Pensacoal	2013-04-26 17:43:37.733732	2013-04-26 17:43:37.733732	11-29-2011_6-00-52_PM.png	image/png	115082	2013-04-26 17:43:35.626368	3	Bishop	Hyde Park Charasmatic	t
10		(772) 465-0036			2013-05-21 20:37:49.489547	2013-05-21 20:40:59.084657	banks.jpg	image/jpeg	73513	2013-05-21 20:40:57.799028	45	Pastor	Resurrection Life Family Worship Center	t
11	\N		2001 Avenue D, Fort Pierce, Fl 34950		2013-05-24 17:25:47.754213	2013-05-24 17:25:47.754213	DSC_7911.JPG	image/jpeg	3732890	2013-05-24 17:25:43.724656	54	Pastor		t
4					2013-04-28 02:44:09.899918	2013-04-28 02:45:31.250887	\N	\N	\N	\N	20	Pastor		t
13					2013-05-26 20:47:13.262735	2013-05-26 20:47:13.262735	\N	\N	\N	\N	56	Pastor	Non Denomination	f
7					2013-05-05 11:39:57.340596	2013-05-05 11:39:57.340596	\N	\N	\N	\N	21	Pastor		t
6		9193968855	P.O. BOX 134 Goldsboro, NC 27530		2013-05-04 01:33:41.525728	2013-05-06 12:05:39.268137	Tim_Cynthia.jpg	image/jpeg	38512	2013-05-06 12:05:36.386988	22	Pastor		t
5					2013-04-28 02:47:03.498718	2013-05-09 02:05:17.708772	DSCF0774.JPG	image/jpeg	3459630	2013-05-09 02:05:14.305608	18	Pastor		t
12		210-324-0985			2013-05-26 20:46:40.430709	2013-05-26 22:00:25.353418	image.jpg	image/jpeg	854026	2013-05-26 20:46:36.97011	56	Minister		t
2	<p>Greetings, I&#39;m Apostle Vernon Whitaker with my wife Prophetess Betty, we oversee First Church Five-Fold Fellowship Ministries Inc. located in Port Saint Lucie, FL. 34984.&nbsp;Through the blessings of God, Apostle Rick Carraway and I have worked on several projects together. Apostle Rick shared the idea of Christian Event Link with&nbsp;me. After hearing Apostle Rick vision God opened my eyes to the vision of Apostle Rick putting together a website that would bring Christians together&nbsp;and allow pastors to imput information about their&nbsp;worship services and ministry plans. The nature of our friendship and working relations prompted him to reccommend that I share in the vision of Christian Event Link Inc.</p>\r\n	561-718-2361	1689 SW Biltmore Street Port Saint Lucie FL 34984		2013-04-23 01:15:58.919496	2013-05-17 00:41:13.984482	Apostle_Whitaker.jpg	image/jpeg	67401	2013-05-17 00:33:55.280685	15	Pastor	None-Denomination	t
9	\N	34234	asdfasdf	asdfasdf	2013-05-20 14:06:22.89703	2013-05-20 14:06:22.89703	mel_and_i_fixed.jpg	image/jpeg	216696	2013-05-20 14:06:19.814819	1	Preacher	dfasdf	t
14	<p><a href="http://www.homi.me">http://www.homi.me</a></p>\r\n	773 616 1951	8600 South Bishop Chicago, IL 60620		2013-05-28 21:00:07.121377	2013-05-28 21:10:13.871733	Apostle_Brinson_III_Picture.jpg	image/jpeg	167848	2013-05-28 21:00:05.074095	59		Hope Outreach Ministries Inc./Hope Outreach Ministries International Inc.	t
15	<p>Pastor Deanna Smith was born in Philadelphia, PA.&nbsp; She is married to Apostle Leonard A. Smith, Sr., Prelate of UICI (United In Christ International, Inc).&nbsp; From this union came four children and five grandchildren.&nbsp; Apostle Smith and Deanna reside in Stuart, Florida where she is the Co-Pastor of Wings of Deliverance Outreach Center.</p>\r\n\r\n<p>Deanna has blazed the trail for many Women Fellowships.&nbsp; She was one of the earliest Evangelists to bring inner healing to the body of Christ, bringing hidden abuse of Christian relationships to&nbsp; the forefront.&nbsp; Deanna&nbsp; was&nbsp; called&nbsp; to&nbsp;be a Mother of Zion at the early age of 29, counseling some of the most prominent male leaders across the country.&nbsp; Deanna is a visionary, and in her early ministry saw the need for an organization that would be comprised of individuals demonstrating the love of Christ in the lives of others.&nbsp; In 1983, Deanna along with a group of women founded the International Christian Men &amp; Women Fellowship, Inc. (ICM&amp;WF).&nbsp; This organization was built on the principles of love; caring and helping those of broken spirit and in need both spiritually as well as physically.&nbsp; ICM&amp;WF is not a church but a non-profit, nondenominational organization that serves as a support group to churches throughout the United States and abroad.</p>\r\n\r\n<p>Through vehicles such as revivals, conferences, workshops, seminars, crusades, street ministries, Deanna has utilized these channels in reaching the unsaved.&nbsp; She preaches restoration and inner healing to those suffering from heartache, pain and distress.&nbsp; Equipped with God-given talents and a special anointing, Deanna encourages the hearts of many with a clear&nbsp; message that inspires and uplifts all that come in her path.&nbsp; At a time when boundaries are vague and the standard of righteousness is being restructured, she remains true to the principle and preaches a Recall to Holiness.&nbsp;&nbsp; Deanna believes the walls of prejudices must be broken down along with tradition and denominations.&nbsp; For the bible tells us to &ldquo;Be ye Holy as He is Holy&rdquo;.&nbsp; She stands on the principles of holiness, which her spiritual parents Bishop Ellis and Mother Rosalie Campbell taught her.&nbsp; To God be the glory for such a foundation.&nbsp; She is unmovable as proclaimed in Isaiah 61:1.</p>\r\n\r\n<p>Deanna is spearheading a&nbsp;project,&nbsp;the Multi-Cultural Center to be built in Martin County, Florida.&nbsp; This Center will be built based on the Apostolic Doctrine where none shall lack.&nbsp; As a networker, Deanna partner with various agencies throughout the state of Florida; i.e. Department of Children Services, Florida Department of Juvenile Justice, the Governor&#39;s One Church One Child Program located in Tallahassee, FL and many more.&nbsp;&nbsp;&nbsp;&nbsp; Deanna is a licensed Ordained Minister and has received numerous degrees (Honorary Doctoral Degree &ndash; Christian Counseling and Humanitarian of Missions), certificates and appointments, namely from the National Chaplain Association.&nbsp; Deanna has a worldwide radio ministry that is aired weekly on www.WJFP.com. Kingdom building is Deanna&rsquo;s number one priority.&nbsp;</p>\r\n	(772) 288-4595                                                                                        	4551 SE Geraldine Street, Stuart, FL  34994                                                                                   		2013-05-31 00:22:00.311065	2013-05-31 00:28:02.960557	Deanna_Web_Pix.png	image/x-png	170235	2013-05-31 00:23:18.404386	62	Pastor	Wings of Deliverance Outreach	t
16	<p>Apostle Leonard A. Smith, Sr. was born in Daytona Beach, FL and raised in Hobe Sound, FL.&nbsp; At a young adult, he relocated to New Jersey where he was a professional gospel singer and recording artist with the Heavenly Aires out of Plainfield, NJ.&nbsp; Apostle Smith gave his life to Christ, under the leadership if his Spiritual Father, the late Bishop J.P. Lucas, Diocese of the Mt. Calvary COGIC.&nbsp; Apostle Smith worked in outreach ministries, heading soup kitchens and Prayer services for the homeless and needy.&nbsp;&nbsp; He accepted his calling into the ministry as an evangelist, running revivals in the Northern Regions of the United States.</p>\r\n\r\n<p>In 1979, Apostle Smith married Deanna, two (2) daughters, one (1) son, five (5) grandchildren and a host of foster children that completes this union.&nbsp;&nbsp; Having served under the late Dr. E.C. Campbell, Star Light Holiness Church of the Apostolic Faith, Philadelphia, PA, Apostle Smith served as a Sunday School Teacher and Superintendant, District Elder and Praise Worship Leader.&nbsp; He was elevated being ordained and appointed pastor of Star Light Holiness Church #8.&nbsp; Under the Ecclesiastes Board of Bishops, Apostle was consecrated Bishop.&nbsp; Again, elevation came and in 2001, Apostle Smith was consecrated as an Apostle.</p>\r\n\r\n<p>Apostle Smith is retired from the Martin County School Board.&nbsp; He is the author of &ldquo;Who Can I Talk Too&rdquo;, the founder of the Tough Love Program and A Few Good Men.&nbsp; Apostle Smith is a member of Prayer Partners of America out of Detroit, MI.&nbsp; He has also earned his Doctorate Degree in Christian Counseling and has two (2) Honorary Doctorate Degrees.&nbsp;&nbsp; </p>\r\n	772-834-5086	4551 SE Geraldine Street, Stuart, FL  34994                                                                                   		2013-05-31 00:49:29.673397	2013-05-31 00:50:31.321178	Apostle_Smith_Web_Pix.png	image/x-png	361222	2013-05-31 00:49:27.118193	64	Pastor	United In Christ International (Wings Of Deliverance)	t
\.


--
-- Name: pastors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('pastors_id_seq', 16, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY schema_migrations (version) FROM stdin;
20130310082926
20130310085325
20130310220634
20130315180104
20130315180105
20130315180106
20130315180752
20130316175858
20130318174324
20130318174525
20130318185010
20130318185146
20130318185837
20130318190548
20130318202224
20130318204122
20130319185653
20130319191601
20130319191743
20130319192707
20130319204657
20130319205157
20130320145319
20130327140458
20130327220038
20130328004016
20130330001648
20130330002033
20130330003448
20130408001148
20130410224334
20130410224620
20130411135805
20130411164529
20130411165348
20130415130428
20130422164850
20130422165224
20130422174240
20130422174904
20130422181854
20130425151151
20130425151152
20130425151153
20130425151154
20130425151155
20130425151156
20130425151157
20130425151158
20130425151159
20130425151160
20130425151161
20130425151162
20130425151163
20130425151164
20130425151165
20130425151166
20130425151167
20130425151168
20130425151169
20130425151170
20130425151171
20130425151172
20130425151173
20130425151174
20130425151175
20130425151176
20130425151177
20130425151178
20130425151179
20130425151180
20130507221702
20130507234111
20130516201155
20130516201214
20130517212210
20130522213806
\.


--
-- Data for Name: shippings; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY shippings (id, event_id, name, short_description, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: shippings_events; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY shippings_events (shipping_id, event_id) FROM stdin;
\.


--
-- Name: shippings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('shippings_id_seq', 1, false);


--
-- Data for Name: taggings; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY taggings (id, tag_id, taggable_id, taggable_type, tagger_id, tagger_type, context, created_at) FROM stdin;
\.


--
-- Name: taggings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('taggings_id_seq', 1, false);


--
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY tags (id, name) FROM stdin;
\.


--
-- Name: tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('tags_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: cel
--

COPY users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at, first_name, last_name, provider, uid, is_admin, is_active, forem_admin, forem_state, forem_auto_subscribe) FROM stdin;
12	mario.r.vallejo@gmail.com	$2a$10$TV0g/MUGdG5mWpVGWTVCI.kea96ZzwtZHTWr4QK8XC.GylarKPj.G	\N	\N	\N	13	2013-04-03 22:44:12.591323	2013-04-03 22:43:44.90139	187.178.139.159	187.178.139.159	2013-04-03 21:47:00.978854	2013-04-03 22:44:12.592634	Mario	Vallejo	facebook	1456577996	f	t	f	pending_review	f
16	ssjkclan@gmail.com	$2a$10$p8Mk6vFuhQ7FKv7xUUHaWOy2qw2Ll4JkEulKZdROpzg.XchvehHnC	\N	\N	\N	1	2013-04-05 18:53:42.700843	2013-04-05 18:53:42.700843	72.216.11.251	72.216.11.251	2013-04-05 18:53:42.367195	2013-04-05 18:53:42.705464	Stephanie	Pinkston	\N	\N	f	t	f	pending_review	f
21	ectamez5@hotmail.com	$2a$10$2o5G3766Ampb6MyVcwToTufE6pFXukYtJFil1AqO4ez75Vn/bSn9y	\N	\N	\N	1	2013-05-01 04:50:03.207283	2013-05-01 04:50:03.207283	108.249.180.171	108.249.180.171	2013-05-01 04:50:03.045287	2013-05-01 04:50:03.208338	Eli Christopher	Tamez	facebook	508877184	f	t	f	pending_review	f
53	krista@prioritymarketing.com	$2a$10$boMz0U/xGsIQQd7zbQ9.DOAqsqPNe5cba0O3n670ml4/dybwB6mu6	\N	\N	\N	1	2013-05-24 13:54:40.948765	2013-05-24 13:54:40.948765	75.144.105.241	75.144.105.241	2013-05-24 13:54:40.87292	2013-05-24 13:54:40.949787	Krista 	Cartee	\N	\N	f	t	f	pending_review	f
2	admin@example.com		\N	\N	\N	0	\N	\N	\N	\N	2013-03-30 02:39:29.192795	2013-04-26 17:41:22.150675	Testing	Administrator	\N	\N	t	t	t	pending_review	f
56	shenry23@satx.rr.com	$2a$10$8nARUOb2vnt4nF9c6mhbveBHBhfNt.YbTCl.Sgjo0qGmqekPdeaca	\N	\N	\N	1	2013-05-26 20:39:56.224969	2013-05-26 20:39:56.224969	67.11.52.137	67.11.52.137	2013-05-26 20:39:56.121106	2013-05-26 20:39:56.226041	Shelton	Henry	facebook	100006024063301	f	t	f	pending_review	f
18	shannonscarr1@yahoo.com	$2a$10$O8kOr07OhddCebM8pe0pEOBJoOCh8v6l2g9X72oxizpKHk1/U4yvG	\N	\N	\N	2	2013-05-06 11:06:22.758034	2013-04-24 02:05:27.914641	108.251.233.59	108.251.233.59	2013-04-24 02:05:27.706877	2013-05-06 11:06:22.759535	Shannon	Carraway	\N	\N	f	t	f	pending_review	f
54	purefire56@gmail.com	$2a$10$GsFyWjmX3.tX5KczbeIP2.M/95dd5hOiUdqOnf/rVW8Vhv2JiU.Nu	\N	\N	\N	2	2013-05-24 17:15:28.263133	2013-05-24 17:08:04.620222	208.54.85.173	208.54.85.173	2013-05-24 17:08:03.79155	2013-05-24 17:15:28.264991	PureFire ministries		\N	\N	f	t	f	pending_review	f
15	firstchurch12@gmail.com	$2a$10$edSfVbSe/5OEW5WlUHJTw.LFEXaHNgKZvbE5hemjass7bHqmnKnOq	\N	\N	\N	40	2013-05-30 02:17:44.454083	2013-05-29 11:00:06.280878	50.154.204.72	50.154.204.72	2013-04-04 15:51:13.299052	2013-05-30 02:17:44.456081	Vernon	Whitaker	facebook	100003200506155	t	t	f	pending_review	f
45	information@trbanks.com	$2a$10$jUtvdMlCcQsmLOYepQxpJOZFkm8bepPC5FKsrXLaZtRbTAEFTdzeC	\N	\N	\N	2	2013-05-21 20:45:14.754647	2013-05-21 20:36:33.032058	72.216.0.210	72.216.0.210	2013-05-21 20:36:33.022149	2013-05-21 20:45:14.770626	Treble	Banks	\N	\N	f	t	f	pending_review	f
46	videojme@gmail.com	$2a$10$O/L/hyBDOdsOsD414BycPucK0tVn/kdoTzwKUaFpImb2MTwNW/oDS	\N	\N	\N	1	2013-05-21 22:24:10.578123	2013-05-21 22:24:10.578123	96.19.70.111	96.19.70.111	2013-05-21 22:24:10.518649	2013-05-21 22:24:10.58086	Jamey	Foster	facebook	609906063	f	t	f	pending_review	f
1		$2a$10$IYhS2BZQXyO/kNbXUtPl1ujzYtkq2XL9WidmADPg.9gFgn0mWY3Nu	\N	\N	\N	16	2013-05-20 14:05:59.396512	2013-05-17 14:50:58.409529	187.178.139.159	187.178.139.159	2013-03-30 02:37:13.102913	2013-05-20 14:05:59.398073	Axolote De Acción		twitter	117578848	t	t	t	pending_review	f
22	timothywhitfield79@gmail.com	$2a$10$iQhh80iuHqZ0ZzVq/xO.iuDBSi3LlBeXMHJ3.iS.raYViTAgiBlPe	JtK74zSd7Lpr4XzuHR8Y	2013-05-23 21:29:44.312333	\N	2	2013-05-23 21:30:15.711659	2013-05-04 01:27:37.503063	74.167.231.22	108.251.233.59	2013-05-04 01:27:37.271966	2013-05-23 21:30:15.713426	Timothy	Whitfield	\N	\N	f	t	f	pending_review	f
55	jeanette_dunlap@yahoo.com	$2a$10$9PGbskFrrIRLDOEcLjnKS..dSUUnbTpQFuC.Qbxvrmn1IOBdRvV0C	\N	\N	\N	1	2013-05-26 01:10:14.333054	2013-05-26 01:10:14.333054	69.35.184.58	69.35.184.58	2013-05-26 01:10:14.101281	2013-05-26 01:10:14.33474	Janette	Dunlap	\N	\N	f	t	f	pending_review	f
59	apostledoc7@aol.com	$2a$10$M2YRos/iqWdK/i6Ay9Q.eO7Q8o1t0yg1pBnYzjxpO6VtBBpwmnnPy	\N	\N	\N	4	2013-05-28 20:51:37.602003	2013-05-28 20:40:55.536267	99.105.64.101	99.105.64.101	2013-05-28 20:33:17.114328	2013-05-28 20:51:37.654987	Apostle Sylvester	Brinson III	\N	\N	f	t	f	pending_review	f
57	mcdanielbarbara@hotmail.com	$2a$10$O4JJJQgZMlm7KmSlaUgl5OgeiCb0s3rioIGScC9IACZ9rdEdnpX1u	\N	\N	\N	1	2013-05-27 21:25:58.593675	2013-05-27 21:25:58.593675	70.238.155.92	70.238.155.92	2013-05-27 21:25:58.492836	2013-05-27 21:25:58.594718	Barbara	McDaniel	facebook	1257977178	f	t	f	pending_review	f
20	whitaker913391@bellsouth.net	$2a$10$OqhMSIhZS5K5panBXACPLu89Obt0zpIfx.nV9HpTRRNA.ErO/1Pya	\N	\N	\N	1	2013-04-26 16:58:04.610681	2013-04-26 16:58:04.610681	216.81.178.90	216.81.178.90	2013-04-26 16:58:04.546371	2013-05-24 09:28:51.968289	Betty 	Whitaker	\N	\N	f	t	f	pending_review	f
60	resthope@aol.com	$2a$10$geFdD14p1rXRmr0UxI9QvOi4j2vzMSZrtzX2RGwmC/5RLjVhTSq/q	\N	\N	\N	1	2013-05-28 21:12:00.489668	2013-05-28 21:12:00.489668	99.105.64.101	99.105.64.101	2013-05-28 21:12:00.424654	2013-05-28 21:12:00.491183	Apostle	Brinson	facebook	751889895	f	t	f	pending_review	f
61	kenyia28johnson@yahoo.com	$2a$10$5xL/w2xh8Ijbv0CuqhkhW.Z2UepsTFAayWYYi4caop48xiFe3vZxy	\N	\N	\N	1	2013-05-30 11:12:16.022985	2013-05-30 11:12:16.022985	98.254.172.187	98.254.172.187	2013-05-30 11:12:15.676596	2013-05-30 11:12:16.029603	Kenyia 	Johnson	\N	\N	f	t	f	pending_review	f
64	greeneb11@aol.com	$2a$10$8vpLIP.omLm.XLJ3YvMDVe90rpZMNtV4Bi.fVpJn/knskZPdcd65u	\N	\N	\N	1	2013-05-31 00:47:00.823097	2013-05-31 00:47:00.823097	96.228.234.22	96.228.234.22	2013-05-31 00:47:00.81915	2013-05-31 00:47:00.824114	Leonard	Smith	\N	\N	f	t	f	pending_review	f
58	perfectpro4u@gmail.com	$2a$10$sDSEoZ5HdrxQJsNjZH042eoCAbp3aJK6U/LcDH8VNCOix4wo31ElC	\N	\N	\N	1	2013-05-28 18:47:04.907365	2013-05-28 18:47:04.907365	208.76.113.2	208.76.113.2	2013-05-28 18:47:04.171146	2013-05-28 18:47:05.047386	LaShaun	Jenice	facebook	100002405592954	f	t	f	pending_review	f
62	toughlove2010@att.net	$2a$10$O5lKkMWE3C22wMvRzHi9g./URUl9QdInx0z0RSwW3jy28HqjZzQHC	\N	\N	\N	1	2013-05-31 00:03:21.293813	2013-05-31 00:03:21.293813	96.228.234.22	96.228.234.22	2013-05-31 00:03:21.145336	2013-05-31 00:03:21.29547	Deanna	Smith	\N	\N	f	t	f	pending_review	f
3	shawnpinkston@gmail.com	$2a$10$GJ3isdeNVTzVuJ3CqxW/cu9FyT2FM4tXT77ETXkRQiT6sd0vqyeM2	\N	\N	\N	55	2013-05-31 20:55:13.217058	2013-05-30 20:35:59.801067	72.216.7.83	72.216.7.83	2013-04-01 02:11:54.964696	2013-05-31 20:55:13.22784	Shawn	Pinkston	facebook	100001000512871	t	t	t	approved	f
13	scarraw1@bellsouth.net	$2a$10$2kkfVtpIaFVUK5/oqQTzGeODvJtli/eeOLNHLbhJhSh6qj1VSvGpe	\N	\N	\N	123	2013-06-01 02:35:45.814317	2013-05-31 16:37:09.893105	108.251.233.59	174.141.208.105	2013-04-04 04:21:07.86261	2013-06-01 02:35:46.635235	Ricky	Carraway	facebook	510469149	t	t	t	approved	f
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cel
--

SELECT pg_catalog.setval('users_id_seq', 64, true);


--
-- Name: admin_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY active_admin_comments
    ADD CONSTRAINT admin_notes_pkey PRIMARY KEY (id);


--
-- Name: attendees_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY attendees
    ADD CONSTRAINT attendees_pkey PRIMARY KEY (id);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: clients_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: forem_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY forem_categories
    ADD CONSTRAINT forem_categories_pkey PRIMARY KEY (id);


--
-- Name: forem_forums_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY forem_forums
    ADD CONSTRAINT forem_forums_pkey PRIMARY KEY (id);


--
-- Name: forem_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY forem_groups
    ADD CONSTRAINT forem_groups_pkey PRIMARY KEY (id);


--
-- Name: forem_memberships_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY forem_memberships
    ADD CONSTRAINT forem_memberships_pkey PRIMARY KEY (id);


--
-- Name: forem_moderator_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY forem_moderator_groups
    ADD CONSTRAINT forem_moderator_groups_pkey PRIMARY KEY (id);


--
-- Name: forem_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY forem_posts
    ADD CONSTRAINT forem_posts_pkey PRIMARY KEY (id);


--
-- Name: forem_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY forem_subscriptions
    ADD CONSTRAINT forem_subscriptions_pkey PRIMARY KEY (id);


--
-- Name: forem_topics_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY forem_topics
    ADD CONSTRAINT forem_topics_pkey PRIMARY KEY (id);


--
-- Name: forem_views_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY forem_views
    ADD CONSTRAINT forem_views_pkey PRIMARY KEY (id);


--
-- Name: newsletters_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY newsletters
    ADD CONSTRAINT newsletters_pkey PRIMARY KEY (id);


--
-- Name: pastors_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY pastors
    ADD CONSTRAINT pastors_pkey PRIMARY KEY (id);


--
-- Name: shippings_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY shippings
    ADD CONSTRAINT shippings_pkey PRIMARY KEY (id);


--
-- Name: taggings_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY taggings
    ADD CONSTRAINT taggings_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: cel; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_active_admin_comments_on_author_type_and_author_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_author_type_and_author_id ON active_admin_comments USING btree (author_type, author_id);


--
-- Name: index_active_admin_comments_on_namespace; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_namespace ON active_admin_comments USING btree (namespace);


--
-- Name: index_admin_notes_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_admin_notes_on_resource_type_and_resource_id ON active_admin_comments USING btree (resource_type, resource_id);


--
-- Name: index_attendees_on_event_id_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_attendees_on_event_id_id ON attendees USING btree (event_id);


--
-- Name: index_attendees_on_user_id_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_attendees_on_user_id_id ON attendees USING btree (user_id);


--
-- Name: index_events_on_category_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_events_on_category_id ON events USING btree (category_id);


--
-- Name: index_events_on_slug; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_events_on_slug ON events USING btree (slug);


--
-- Name: index_events_on_user_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_events_on_user_id ON events USING btree (user_id);


--
-- Name: index_forem_categories_on_slug; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE UNIQUE INDEX index_forem_categories_on_slug ON forem_categories USING btree (slug);


--
-- Name: index_forem_forums_on_slug; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE UNIQUE INDEX index_forem_forums_on_slug ON forem_forums USING btree (slug);


--
-- Name: index_forem_groups_on_name; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_groups_on_name ON forem_groups USING btree (name);


--
-- Name: index_forem_memberships_on_group_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_memberships_on_group_id ON forem_memberships USING btree (group_id);


--
-- Name: index_forem_moderator_groups_on_forum_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_moderator_groups_on_forum_id ON forem_moderator_groups USING btree (forum_id);


--
-- Name: index_forem_posts_on_reply_to_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_posts_on_reply_to_id ON forem_posts USING btree (reply_to_id);


--
-- Name: index_forem_posts_on_state; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_posts_on_state ON forem_posts USING btree (state);


--
-- Name: index_forem_posts_on_topic_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_posts_on_topic_id ON forem_posts USING btree (topic_id);


--
-- Name: index_forem_posts_on_user_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_posts_on_user_id ON forem_posts USING btree (user_id);


--
-- Name: index_forem_topics_on_forum_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_topics_on_forum_id ON forem_topics USING btree (forum_id);


--
-- Name: index_forem_topics_on_slug; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE UNIQUE INDEX index_forem_topics_on_slug ON forem_topics USING btree (slug);


--
-- Name: index_forem_topics_on_state; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_topics_on_state ON forem_topics USING btree (state);


--
-- Name: index_forem_topics_on_user_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_topics_on_user_id ON forem_topics USING btree (user_id);


--
-- Name: index_forem_views_on_topic_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_views_on_topic_id ON forem_views USING btree (viewable_id);


--
-- Name: index_forem_views_on_updated_at; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_views_on_updated_at ON forem_views USING btree (updated_at);


--
-- Name: index_forem_views_on_user_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_forem_views_on_user_id ON forem_views USING btree (user_id);


--
-- Name: index_taggings_on_tag_id; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_taggings_on_tag_id ON taggings USING btree (tag_id);


--
-- Name: index_taggings_on_taggable_id_and_taggable_type_and_context; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE INDEX index_taggings_on_taggable_id_and_taggable_type_and_context ON taggings USING btree (taggable_id, taggable_type, context);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: cel; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

