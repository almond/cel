class Pastor < ActiveRecord::Base
  belongs_to :user
  accepts_nested_attributes_for :user

  TITLES = [
		["Pastor", "Pastor"],
		["Preacher", "Preacher"],
		["Bishop", "Bishop"],
		["Minister", "Minister"],
		["Priest","Priest"]
	]



  has_attached_file :profile_pic, :styles => { :small => "150x100>", :medium => "400x400>", :large => "800x530>" }

  scope :active, where(:active => true)
  attr_accessible :address_one, :address_two, :bio, :email,
                  :name, :phone, :profile_pic, :active, :slug, :user,
                  :user_id, :title, :congregation, :user_attributes,
                  :is_active

  def to_s
  	"#{self.title} #{self.user.first_name}"
  end

	def self.search_for(params, current_user)
		pastors = []
		if params[:short_description_or_name].present?
			pastors = Pastor.joins(:user).where{}
			# Split the pastor. Let's see if there'e a title
			split_query = params[:short_description_or_name].split(" ")

			# This will need to go a bit deeper
			if self::TITLES.find_all{ |item| item[0] == split_query[0].capitalize }.any?
				denomination = split_query[0].capitalize
				pastors = pastors.where{ ( title =~ "%#{ my{ denomination } }%" ) }
				split_query.delete_at(0)
			end

			if split_query.length == 2
				# Assume first name and last name
				pastors = pastors.where{ (user.first_name =~ "%#{my{ split_query[0] }}%") | (user.last_name =~ "%#{my{ split_query[1] }}%") }
			end

			if split_query.length == 1
				# Assume just last name
				pastors = pastors.where{ (user.first_name =~ "%#{my{ split_query[0] }}%") | (user.last_name =~ "%#{my{ split_query[0] }}%") }
			end
		end
		pastors
	end
end
