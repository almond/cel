class Newsletter < ActiveRecord::Base
	validates :email,
		:uniqueness => true
  attr_accessible :email
end
