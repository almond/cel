class Client < ActiveRecord::Base
	BANNER_SIZES = [
		["Large Full Banner", "Large Full Banner"],
		["2/3 Top", "2/3 Top"],
		["Top Banner", "Top Banner"],
		["Middle Banner", "Middle Banner"],
		["Bottom Banner", "Bottom Banner"]
	]

	STATES = Event::STATES

	has_attached_file :company_logo, :styles => { :small => "150x100>", :medium => "400x400>", :large => "800x530>" }
	has_attached_file :banner, :styles => { :small => "150x100>", :medium => "400x400>", :large => "800x530>" }

  attr_accessible :address_two, :address_one, :company, :email, :first_name,
  								:last_name, :phone, :website, :city, :state, :zip_code, :banner_size,
  								:banner, :company_logo
end
