class NewslettersController < InheritedResources::Base
	def create
		@newsletter = Newsletter.new
		@newsletter.email = params[:email]
		respond_to do |format|
			if @newsletter.save
				format.html { redirect_to(root_path, :notice => "You have been sucessfully added to the Newsletter") }
				format.json { render :json => @newsletter, :status => :created, :location => root_path }
			else
				format.html { redirect_to(root_path, :notice => "That email has already been registered in the DB") }
				format.json { render :json => @newsletter.errors, :status => :unprocessable_entity }
			end
		end
	end
end
