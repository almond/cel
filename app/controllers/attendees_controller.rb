class AttendeesController < ApplicationController
	def create
		user = User.find_by_id(params[:user_id])
		event = Event.find_by_id(params[:event_id])
		@attendee = Attendee.new(:user_id => user.id, :event_id => event.id)
		respond_to do |format|
			if @attendee.save					
				format.html { redirect_to(event, :notice => "Your attendance to the event has been registered.") }
				format.json { render :json => @attendee, :status => :created, :location => event }
			else
				format.html { redirect_to(event, :notice => "There was an error processing your attendance") }
				format.json { render :json => @attendee.errors, :status => :unprocessable_entity }
			end
		end
	end

	def destroy
		@attendee = Attendee.find(params[:id])
		@event = Event.find(@attendee.event_id)
		@attendee.destroy

		respond_to do |format|
			format.html { redirect_to event_path(@event), :notice => "Your attendance has been cancelled" }
			format.json { render :json => @attendee, :status => :deleted, :location => @event }
		end
	end
end
