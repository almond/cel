class ClientsController < InheritedResources::Base
	def show
		redirect_to(root_path, :notice => "You message has been sent. Please wait for us to get in touch with you")
	end
end
