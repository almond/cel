class ApplicationController < ActionController::Base

  def forem_user
    current_user
  end
  helper_method :forem_user

  protect_from_forgery

  def authenticate_admin_user!
    raise SecurityError unless current_user.try(:is_admin) == true
  end

  def check_privileges!
    flash[:error] = "You have to be logged in"
    redirect_to new_user_session_path unless current_user
  end

  def check_pastor_privileges!
    flash[:error] = "You have to be a pastor to access this part of the site"
    redirect_to new_user_session_path unless current_user and current_user.pastor.present?
  end

  def store_location
    session[:previous_url] = request.fullpath unless request.fullpath =~ /\/users/
  end

  def after_sign_in_path_for(resource)
    session[:previous_url] || root_path
  end

  def after_update_path_for(resource)
    session[:previous_url] || root_path
  end

  rescue_from SecurityError do |exception|
    redirect_to root_path
  end
end
