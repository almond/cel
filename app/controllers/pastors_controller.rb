require 'json'

class PastorsController < InheritedResources::Base
	# before_filter :check_pastor_privileges!, only: [ :index ]
	before_filter :check_privileges!, only: [ :index ]

	def index
		# Check if user is a pastor and redirect to discourse app.
		# Otherwise show the login / create new pastor thing.
		@pastor = Pastor.new

		if current_user and current_user.pastor.present? and current_user.pastor.is_active
			redirect_to(forem.forums_url)
		else
			respond_to do |format|
				format.html
				format.json { render :json => @pastor }
			end
		end

	end

	def create
		@pastor = Pastor.new(params[:pastor])
		@pastor.user = current_user
		@pastor.is_active = true

		respond_to do |format|
			if @pastor.save
				format.html { redirect_to(@pastor, :notice => "Your account has been sucessfully created") }
				format.json { render :json => @pastor, :status => :created, :location => @pastor }
			else
				format.html { render :action => "new" }
				format.json { render :json => @pastor.errors, :status => :unprocessable_entity }
			end
		end
	end

	def show
		@pastor = Pastor.find(params[:id])
		@events = Event.where("user_id = ? AND is_occurrence = ?", @pastor.user.id, false)

		#if ! @events
			#@events = Array.new
		#end

		respond_to do |format|
			format.html
			format.json { render :json => [@pastor, @events] }
		end
	end

	def pastor_events
		start_date = DateTime.strptime(request.GET()[:start], "%s")
		end_date = DateTime.strptime(request.GET()[:end], "%s")
		@pastor = Pastor.find(params[:id])
		@events = Event.where("user_id = ? AND start_date >= ? AND start_date <= ?", @pastor.user.id, start_date, end_date)

		car = []
		for event in @events
			if event.is_occurrence
				color = "#3a87ad"
				event_path_string = ""
			else
				color = "#69a646	"
				event_path_string = event_path(event)
			end

			event_hash = { 
				:id => event.id, 
				:title => event.title,
				:start => event.start_date,
				:end => event.end_date,
				:url => event_path_string,
				:color => color
			}
			car << event_hash
		end

		respond_to do |format|
			format.json { render :json => car }
		end
	end
end
