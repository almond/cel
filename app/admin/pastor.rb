ActiveAdmin.register Pastor do

	index	do
		column :id do |pastor|
			link_to "#{ pastor.id }", pastor
		end
		column :user
		column :title
		column :congregation
		column :phone
		column :profile_pic do |pastor|
			image_tag(pastor.profile_pic.url(:small))
		end
		default_actions
	end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :user, :as => :select
      f.input :title, :as => :select, :collection => Pastor::TITLES
      f.input :congregation
      f.input :phone
      f.input :bio, :as => :html_editor
      f.input :address_one
      f.input :address_two
      f.input :profile_pic, :as => :file, :hint => f.template.image_tag(f.object.profile_pic.url(:small))
      f.input :is_active
    end
    f.buttons
  end
end
