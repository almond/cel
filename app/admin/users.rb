ActiveAdmin.register User do

	index	do
		column :id
		column :first_name
		column :last_name
		column :email
		column :provider
		default_actions
	end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :email
      f.input :first_name
      f.input :last_name
      f.input :is_admin
      f.input :is_active
      f.input :forem_admin
    end
    f.buttons
  end
end
