ActiveAdmin.register Client do
	show do |client|
		attributes_table do
			row :id
			row :first_name
			row :last_name
			row :company
			row :website
			row :email
			row :phone
			row :address_one
			row :address_two
			row :city
			row :state
			row :zip_code
			row :banner_size
			row :company_logo do
				image_tag(client.company_logo.url(:small))
			end
			row :banner do
				image_tag(client.company_logo.url(:small))
			end
		end
	end

	form :html => { :enctype => "multipart/form-data" }	do |f|
		f.inputs do
			f.input :first_name
			f.input :last_name
			f.input :company
			f.input :website
			f.input :email
			f.input :phone
			f.input :address_one
			f.input :address_two
			f.input :city
			f.input :state, :as => :select, :collection => Client::STATES
			f.input :zip_code
			f.input :banner_size, :as => :select, :collection => Client::BANNER_SIZES
			f.input :company_logo, :as => :file,
														 :hint => f.template.image_tag(f.object.company_logo.url(:small))
			f.input :banner, :as => :file,
															:hint => f.template.image_tag(f.object.banner.url(:small))
		end
	end
end
