// CKEDITOR.config.toolbar = 'Basic';
CKEDITOR.config.toolbar = [
	[ "Source", "-", "Bold", "Strike", "Italic", "RemoveFormat", "Link", "Unlink", "Anchor",
	"Bulleted List", "Numbered List", "Indent",
	"Outdent"],
	["Styles", "Format"]
];

CKEDITOR.config.width = "520px";