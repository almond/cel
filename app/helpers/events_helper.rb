module EventsHelper
	def event_link_user(event)
		if event.user
			if event.user.pastor.present?
				link_to event.user.get_full_name, event.user.pastor
			else
				event.user.get_full_name
			end
		else
			"<DELETED>"
		end
	end

	def event_has_user(event)
		if event.user
			true
		else
			false
		end
	end
end
