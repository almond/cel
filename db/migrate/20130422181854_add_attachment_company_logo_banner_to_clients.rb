class AddAttachmentCompanyLogoBannerToClients < ActiveRecord::Migration
  def self.up
    change_table :clients do |t|
      t.attachment :company_logo
      t.attachment :banner
    end
  end

  def self.down
    drop_attached_file :clients, :company_logo
    drop_attached_file :clients, :banner
  end
end
