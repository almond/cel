class AddIsActiveToPastors < ActiveRecord::Migration
  def change
    add_column :pastors, :is_active, :boolean
  end
end
