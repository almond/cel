class AddFieldsToClients < ActiveRecord::Migration
  def change
    add_column :clients, :address_two, :string
    add_column :clients, :city, :string
    add_column :clients, :state, :string
    add_column :clients, :zip_code, :integer
    add_column :clients, :banner_size, :string
  end
end
