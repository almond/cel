class ChangeIsActiveInPastorsDefault < ActiveRecord::Migration
  def change
  	change_column :pastors, :is_active, :boolean, :default => false
  end
end
