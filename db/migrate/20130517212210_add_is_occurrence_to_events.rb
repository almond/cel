class AddIsOccurrenceToEvents < ActiveRecord::Migration
  def change
    add_column :events, :is_occurrence, :boolean, :default => false
  end
end
