class MoveIsActiveFromPastorsToUsers < ActiveRecord::Migration
	def change
		remove_column :pastors, :is_active
		add_column :users, :is_active, :boolean, :default => true
	end
end
