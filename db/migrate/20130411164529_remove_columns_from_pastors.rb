class RemoveColumnsFromPastors < ActiveRecord::Migration
  def change
  	remove_column :pastors, :name
  	remove_column :pastors, :email
  end
end
