class AddTitleAndCongregationToPastors < ActiveRecord::Migration
  def change
    add_column :pastors, :title, :string
    add_column :pastors, :congregation, :string
  end
end
