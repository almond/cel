class RenameUserIdAndEventIdInTables < ActiveRecord::Migration
    def change
        rename_column :attendees, :user_id_id, :user_id
        rename_column :attendees, :event_id_id, :event_id
    end
end
