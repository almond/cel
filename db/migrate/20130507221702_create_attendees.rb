class CreateAttendees < ActiveRecord::Migration
  def change
    create_table :attendees do |t|
      t.references :user_id
      t.references :event_id

      t.timestamps
    end
    add_index :attendees, :user_id_id
    add_index :attendees, :event_id_id
  end
end
